# ST7735S

### Description
ST7735S display driver written in MicroPython.

### Software Architecture
Module Name ：st7735s<br> 
Module color constant：RED,GREEN,BLUE,WHITE，CYAN,YELLOW,PURPLE,GREY<br> 
The module displays mode constants：PART,NORMAL,SCROLL,IDLE<br>

#### 1、auxiliary function：
rgb565(R, G, B)          #Color generating function<br>

#### 2、 constructor function
st7735s.ST7735S(spi,*,dc,rst,cs,bl=None,width=128,height=160)<br>
st7735s.deinit()<br>

#### 3、 powerful function
1. ST7735S.clear(c=0)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; #Fill the cache with the color c (default black) and display<br>
2. ST7735S.backlight(duty)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#Background brightness adjustment<br>
3. ST7735S.sleep( )&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; #Display goes to sleep<br>
4. ST7735S.wakeup( )&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; #Display wakes up<br>
5. ST7735S.rotate(angle)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#Rotate the screen clockwise, the parameter is the rotation degree, such as 0°, 90°, 180°, 270°<br>
6. ST7735S.setWindow(xs,ys,xe,ye)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#Set the target window for data sending or image drawing, local drawing<br>
7. ST7735S.setDisMode(mode,*p)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#Set partial \ normal \ Scroll \ idle display mode<br>
8. ST7735S.setScrollStart(ys)&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; #In scroll mode, set the start line of the scroll<br>
9. ST7735S.show( )&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;#Send all the contents of the ST7735S to the video memory for full screen display<br>
10. ST7735S.showVPart(ys,ye) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;#Refresh the vertical portion of the display<br>
10. ST7735S.showImage(xs,ys,xe,ye,data:bytes) #Send data(such as camera photos) directly to the screen for display in the specified window<br>
11. ST7735S.bufToBmp(bmpfile:str)&nbsp;&nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;#Save images on the framebuf cache as bmp files in RGB565 format<br>

#### 4、 drawing function
1. ST7735S.fill(c)&nbsp;&nbsp;&nbsp; &nbsp;#Fill the entire ST7735S with the specified color;<br>
2. ST7735S.pixel(x, y [, c] )&nbsp;&nbsp;&nbsp; &nbsp;#Gets or sets the color of the pixel;<br>
3. ST7735S.hline(x, y, w, c)&nbsp;&nbsp;&nbsp; &nbsp;#Draw a horizontal line;<br>
4.  ST7735S.vline(x, y, h, c)&nbsp;&nbsp;&nbsp; &nbsp;#Draw a vertical line;<br>
5.  ST7735S.line(x1, y1, x2, y2, c)&nbsp;&nbsp;&nbsp; &nbsp;#Draw a line segment between two points with a pixel width of 1;<br>
6.  ST7735S.rect(x, y, w, h, c [, f])&nbsp;&nbsp;&nbsp; &nbsp;#Draw rectangle, f parameter determines whether to fill;<br>
7.  ST7735S.ellipse(x, y, xr, yr, c[, f, m])&nbsp;&nbsp;&nbsp; &nbsp;#Draw an ellipse or circle, f determines the fill, m determines the quadrant;<br>
8.  ST7735S.poly(x, y, coords, c[, f])&nbsp;&nbsp;&nbsp; &nbsp;#Draw any polygon, f parameter determines whether to fill;<br>
9.  ST7735S.text(s, x, y [, c])&nbsp;&nbsp;&nbsp; &nbsp;#Draw text, only support 8x8 pixel English characters;<br>
10.  ST7735S.scroll(xstep, ystep)&nbsp;&nbsp;&nbsp; &nbsp;#Move the contents of ST7735S to the given vector;<br>
11.  ST7735S.blit(fbuf, x, y, key=-1, palette=None)&nbsp;&nbsp;&nbsp; &nbsp;#Draw another FrameBuffer at the given coordinates。<br>
12. ST7735S.drawText(text,x,y,fontDB,c,bc,alpha) &nbsp;&nbsp;&nbsp; &nbsp;#Draw Chinese characters or letters from the top left corner (x,y) with bc as the background color<br>
13. ST7735S.drawImage(imgw,imgh,img:bytes,imgformat=RGB565,x=0,y=0) &nbsp;&nbsp;&nbsp; &nbsp;#Draw a bytes image starting at (x,y)<br>




### instructions

```python
#An example of a scrolling display
import st7735s as st
from machine import SPI
from random import randint
import time,gbk
def main():
    #spi uses hardware channel 1. According to the actual situation, modify the connection pins of spi and lcd
    try:
        font=gbk.font16x16()
        spi = SPI(1,baudrate=40000000)  
        lcd=st.ST7735S(spi,dc=2,rst=0,cs=15,bl=12,width=128,height=160)
        lcd.backlight(200)
        lcd.rotate(0)
        #Draws 12 lines of text in the middle scroll area
        for i in range(20,140,10):
            t=f'{i} ' 
            lcd.text(t*(lcd.width//8//len(t)),0,i,st.rgb565(randint(0,255),randint(0,255),randint(0,255)))
        #Draw top and bottom fixed area rectangles and text
        lcd.rect(0,0,lcd.width,20,st.BLUE,1)
        lcd.rect(0,140,lcd.width,20,st.BLUE,1)
        lcd.drawText('顶部固定区TFA',10,2,font,st.YELLOW) 
        lcd.drawText('底部固定区BFA',10,142,font,st.YELLOW)    
        lcd.show()      
         
        #Scroll display for 10 seconds
        lcd.setDisMode(st.SCROLL,20,120,20)
        t,i=time.ticks_ms(),20
        while time.ticks_diff(time.ticks_ms(),t)<10000:
            lcd.setScrollStart(i)
            i=i+1 if i<140 else 20
            time.sleep_ms(30)
        lcd.setDisMode(st.NORMAL)
    finally:
        font.deinit() #Close the database and related files
        lcd.deinit()  #Turn off the pwm output of the backlight  

if __name__=='__main__':
    main()

```



