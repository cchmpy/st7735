import st7735s as st
from machine import SPI
from random import randint
import time,gbk

def main():
    #spi使用硬件通道1。根据实际情况，修改spi和lcd的连接引脚
    try:
        font=gbk.font16x16()
        spi = SPI(1,baudrate=40000000)  
        lcd=st.ST7735S(spi,dc=2,rst=0,cs=15,bl=12,width=128,height=160)
        lcd.backlight(200)
        lcd.rotate(0)
        #绘制中间滚动区12行文本
        for i in range(20,140,10):
            t=f'{i} ' 
            lcd.text(t*(lcd.width//8//len(t)),0,i,st.rgb565(randint(0,255),randint(0,255),randint(0,255)))
        #绘制顶部和底部固定区矩形和文字
        lcd.rect(0,0,lcd.width,20,st.BLUE,1)
        lcd.rect(0,140,lcd.width,20,st.BLUE,1)
        lcd.drawText('顶部固定区TFA',10,2,font,st.YELLOW) 
        lcd.drawText('底部固定区BFA',10,142,font,st.YELLOW)    
        lcd.show()      
         
        #滚动显示10秒
        lcd.setDisMode(st.SCROLL,20,120,20)
        t,i=time.ticks_ms(),20
        while time.ticks_diff(time.ticks_ms(),t)<10000:
            lcd.setScrollStart(i)
            i=i+1 if i<140 else 20
            time.sleep_ms(30)
        lcd.setDisMode(st.NORMAL)
    finally:
        font.deinit() #关闭数据库和相关文件
        lcd.deinit()  #关闭背光的pwm输出
    
if __name__=='__main__':
    main()


