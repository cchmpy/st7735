import st7735s as st
from machine import SPI
import time,camera,random,gbk
from random import randint


def test_writedata(lcd:st.ST7735S):
    '''向帧缓存写入文字'''
    i=0
    while lcd.height-i>10:
        t=f'{i} ' 
        lcd.text(t*(lcd.width//8//len(t)),0,i,st.rgb565(randint(0,255),randint(0,255),randint(0,255)))
        i+=10  
#类定义，用于演示小球在箱体运动的动画
class ball:
    def __init__(self,x:int=0,y:int=0,dx:bool=1,dy:bool=1,r:int=10,c:int=st.RED):
        '''x,y:圆心坐标
           dx,dy:运动方向
           r,c:半径、颜色        
        '''
        self.x,self.y,self.r,self.dx,self.dy,self.c=x,y,r,dx,dy,c
    def move(self,w:int,h:int):
        '''在w*h箱体内运动'''
        if self.x<self.r:   self.dx=1               
        if self.x>w-self.r: self.dx=0
        if self.y<self.r:   self.dy=1
        if self.y>h-self.r: self.dy=0
        self.x=self.x+1 if self.dx else self.x-1
        self.y=self.y+1 if self.dy else self.y-1    

#小球箱体运动动画
def test_ball(lcd,w,h):    
    balls=[]
    for j in range(20):
        balls.append(ball(x=randint(0,w-16),y=randint(0,h-16),r=randint(5,15), \
                          dx=randint(0,1),dy=randint(0,1), \
                          c=st.rgb565(randint(0,255),randint(0,255),randint(0,255))))    
    cnt=0 #帧数
    t0=time.ticks_ms() #计时起点       
    while True:           
        for each in balls: each.move(w,h)
        lcd.fill(0) #填充白色背景st.WHITE
        for each in balls:
            lcd.ellipse(each.x,each.y,each.r,each.r,each.c,1)
        lcd.show()
        cnt+=1
        if time.ticks_diff(time.ticks_ms(),t0)>10000: break
    print('fps:',cnt//10) #打印帧率
    
#测试 滚动模式
def test_scroll(lcd):
    test_writedata(lcd)
    lcd.setDisMode(st.SCROLL,20,100,40)
    lcd.show()    
    #滚动显示5秒
    t0,i=time.ticks_ms(),20
    while time.ticks_diff(time.ticks_ms(),t0)<10000:
        lcd.setScrollStart(i)
        i=(i+1) if i<120 else 20
        time.sleep_ms(20)
    lcd.setDisMode(st.NORMAL)

#测试 直接显示照相机的图像
def test_camera(lcd,img):
    lcd.rotate(270)    
    lcd.showImage(0,0,159,119,img)
    

#测试 显示汉字
def test_drawtext(lcd,font):
    lcd.rotate(0)
    lcd.drawText('顶部固定区TFA',0,0,font,st.RED)
    lcd.drawText('中部滚动区SA',0,70,font,st.GREEN)
    lcd.drawText('底部固定区BFA',0,140,font,st.BLUE)
    lcd.show()

#测试 垂直部分区域刷新显示
def test_partvshow(lcd,font):
    i=0
    t0=time.ticks_ms()
    while time.ticks_diff(time.ticks_ms(),t0)<1000:
        lcd.drawText(f'{i}',10,10,font,st.RED,0,False)
        lcd.showVPart(10,26)
        i+=1
    print(i)
def main():
    try:
        font=gbk.font16x16() #定义汉字库对象
        camera.init(0,format=camera.RGB565,framesize=camera.FRAME_QQVGA,fb_location=camera.PSRAM) #format=camera.GRAYSCALE/RGB565/JPEG/YUV422
        img=camera.capture() 
        spi = SPI(1,baudrate=40000000)  #引脚可能需要修改
        lcd=st.ST7735S(spi,dc=2,rst=0,cs=15,bl=12,width=128,height=160) #引脚可能需要修改
        lcd.backlight(100)
        
#         test_drawtext(lcd,font) #汉字显示
#         test_scroll(lcd) #滚动显示
#         lcd.rotate(0)        
#         lcd.setDisMode(st.PART,20,140)
        test_ball(lcd,lcd.width,lcd.height) #小球箱体运动
#         lcd.fill(st.WHITE)
#         if True:
#             lcd.rotate(270)
#             lcd.drawImage(160,120,img)
#         lcd.rect(0,0,lcd.width-1,lcd.height-1,0)
#         lcd.show()
        test_partvshow(lcd,font) #局部刷新
        
        
        
#         lcd.bufToBmp('d20.bmp') #保存图像       
#         test_camera(lcd,img)#直接显示相机照片       

        
    finally :
        font.deinit() #关闭汉字数据库和相关文件
        camera.deinit()
        print('done')
if __name__=='__main__':
    main()

