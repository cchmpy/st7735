# ST7735S

### 介绍
使用MicroPython编写的ST7735S显示驱动程序。

### 软件架构
模块名称：st7735s<br> 
模块颜色常量：RED,GREEN,BLUE,WHITE，CYAN,YELLOW,PURPLE,GREY<br> 
模块显示模式常量：PART,NORMAL,SCROLL,IDLE<br>

#### 1、 辅助函数：
rgb565(R, G, B)          #颜色生成函数<br>

#### 2、 构造函数
st7735s.ST7735S(spi,*,dc,rst,cs,bl=None,width=128,height=160)<br>
st7735s.deinit()<br>

#### 3、 功能函数
1. ST7735S.clear(c=0)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; #用颜色c（默认黑色）填充缓存，并显示<br>
2. ST7735S.backlight(duty)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#背光灯亮度调节<br>
3. ST7735S.sleep( )&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; #显示屏进入休眠<br>
4. ST7735S.wakeup( )&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; #显示屏被唤醒<br>
5. ST7735S.rotate(angle)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#顺时针旋转屏幕，参数为旋转度数，如0°、90°、180°、270°<br>
6. ST7735S.setWindow(xs,ys,xe,ye)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#设置数据发送或图像绘制的目标窗口，局部绘图<br>
7. ST7735S.setDisMode(mode,*p)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;#设置部分\正常\滚动\空闲显示模式<br>
8. ST7735S.setScrollStart(ys)&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; #滚动模式下，设置滚动的开始线<br>
9. ST7735S.show( )&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;#把ST7735S的内容全部发送到显存，全屏显示<br>
10. ST7735S.showVPart(ys,ye) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;#刷新显示垂直部分区域<br>
11. ST7735S.showImage(xs,ys,xe,ye,data:bytes) #把data&nbsp; (如相机照片)直接发送到屏幕指定窗口显示<br>
12. ST7735S.bufToBmp(bmpfile:str)&nbsp;&nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp;#把framebuf缓存上的图像保存为RGB565格式的bmp文件<br>

#### 4、 绘图函数
1. ST7735S.fill(c)&nbsp;&nbsp;&nbsp; &nbsp;#用指定的颜色填充整个ST7735S；<br>
2. ST7735S.pixel(x, y [, c] )&nbsp;&nbsp;&nbsp; &nbsp;#获取或设置像素的颜色；<br>
3. ST7735S.hline(x, y, w, c)&nbsp;&nbsp;&nbsp; &nbsp;#绘制水平直线；<br>
4.  ST7735S.vline(x, y, h, c)&nbsp;&nbsp;&nbsp; &nbsp;#绘制垂直直线；<br>
5.  ST7735S.line(x1, y1, x2, y2, c)&nbsp;&nbsp;&nbsp; &nbsp;#两点之间绘制线段，像素宽度为1；<br>
6.  ST7735S.rect(x, y, w, h, c [, f])&nbsp;&nbsp;&nbsp; &nbsp;#绘制矩形，f参数决定是否填充；<br>
7.  ST7735S.ellipse(x, y, xr, yr, c[, f, m])&nbsp;&nbsp;&nbsp; &nbsp;#绘制椭圆或圆, f决定填充，m确定象限；<br>
8.  ST7735S.poly(x, y, coords, c[, f])&nbsp;&nbsp;&nbsp; &nbsp;#绘制任意多边形，f参数决定是否填充；<br>
9.  ST7735S.text(s, x, y [, c])&nbsp;&nbsp;&nbsp; &nbsp;#绘制文本，仅支持8x8像素的英文字符；<br>
10.  ST7735S.scroll(xstep, ystep)&nbsp;&nbsp;&nbsp; &nbsp;#将ST7735S的内容移动给定的向量；<br>
11.  ST7735S.blit(fbuf, x, y, key=-1, palette=None)&nbsp;&nbsp;&nbsp; &nbsp;#在给定坐标处绘制另一个FrameBuffer。<br>
12. ST7735S.drawText(text,x,y,fontDB,c,bc,alpha) &nbsp;&nbsp;&nbsp; &nbsp;#从左上角(x,y)处开始绘制汉字或字母，bc为背景色<br>
13. ST7735S.drawImage(imgw,imgh,img:bytes,format=RGB565,x=0,y=0) &nbsp;&nbsp;&nbsp; &nbsp;#从(x,y)处开始绘制bytes类型图像<br>




### 使用说明

```python
import st7735s as st
from machine import SPI
from random import randint
import time,gbk

def main():
    #spi使用硬件通道1。根据实际情况，修改spi和lcd的连接引脚
    try:
        font=gbk.font16x16()
        spi = SPI(1,baudrate=40000000)  
        lcd=st.ST7735S(spi,dc=2,rst=0,cs=15,bl=12,width=128,height=160)
        lcd.backlight(200)
        lcd.rotate(0)
        #绘制中间滚动区12行文本
        for i in range(20,140,10):
            t=f'{i} ' 
            lcd.text(t*(lcd.width//8//len(t)),0,i,st.rgb565(randint(0,255),randint(0,255),randint(0,255)))
        #绘制顶部和底部固定区矩形和文字
        lcd.rect(0,0,lcd.width,20,st.BLUE,1)
        lcd.rect(0,140,lcd.width,20,st.BLUE,1)
        lcd.drawText('顶部固定区TFA',10,2,font,st.YELLOW) 
        lcd.drawText('底部固定区BFA',10,142,font,st.YELLOW)    
        lcd.show()      
         
        #滚动显示10秒
        lcd.setDisMode(st.SCROLL,20,120,20)
        t,i=time.ticks_ms(),20
        while time.ticks_diff(time.ticks_ms(),t)<10000:
            lcd.setScrollStart(i)
            i=i+1 if i<140 else 20
            time.sleep_ms(30)
        lcd.setDisMode(st.NORMAL)
    finally:
        font.deinit() #关闭数据库和相关文件
        lcd.deinit()  #关闭背光的pwm输出
    
if __name__=='__main__':
    main()
```


